var d3 = require("d3");

//SVG dimensions
var width = 960,
 height = 500;
 
 
 var svg = d3.select("body").append("svg")
 .attr('xmlns', 'http://www.w3.org/2000/svg')
 .attr("width", width)
 .attr("height", height);
 
 //add css stylesheet
var svg_style = svg.append("defs")
 .append('style')
 .attr('type','text/css');

//text of the CSS stylesheet below -- note the multi-line JS requires 
//escape characters "\" at the end of each line
var css_text = "<![CDATA[ \
      .someclass { \
          fill: none; \
      } \
  ]]> ";

svg_style.text(css_text);