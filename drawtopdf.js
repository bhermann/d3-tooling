//require modules
var d3 = require("d3");

require("./" + process.argv[2]);

var convert = require('child_process').spawn("rsvg-convert", ["--format=pdf"])

convert.stderr.on('data', function (data) {
	process.stderr.write(data);
});

convert.stdout.on('data', function (data) {
	process.stdout.write(data);
});

convert.stdin.write(d3.select('body').html());
convert.stdin.end();

